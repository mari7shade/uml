﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UML
{
    class Employee
    {
        string Familia;
        string Imia;
        public Employee(string familia, string imia)
        {
            Familia = familia;
            Imia = imia;
        }
        public void ReturnInfo()
        {
            Console.WriteLine(Familia);
            Console.WriteLine(Imia);
        }
    }
    class Post
    {
        public Employee sotrydnik { get; set; }
        public string nazvanieDoljnosti { get; set; }
        public decimal zarplata { get; set; }
    }

    class Organozation
    {
        public Post[] doljnosti;
        public string NameOrganization;
        public decimal Budjet;
        public Organozation(string nameOrganization, decimal budjet)
        {
            NameOrganization = nameOrganization;
            Budjet = budjet;
        }
        public void VuplataZarplat()
        {
            decimal calari = 0;
            for (int i = 0; i<doljnosti.Length; i++)
            {
                calari = calari + doljnosti[i].zarplata;
            }
            Console.WriteLine("Потрібно виплатити сотрудникам за місяць: " + calari);
        }
        public int RabotaMesac(string project)
        {

            if (project == "done")
            {
                return 10000;
            }
            else if (project == "continues")
            {
                decimal calari = 0;
                for (int i = 0; i < doljnosti.Length; i++)
                {
                    calari = calari + doljnosti[i].zarplata;
                }
                return -(int)calari;
            }
            else return 0;
        }
        
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Organozation MAC = new Organozation("MAC", 25500);
            Console.WriteLine("Назва організації: " + MAC.NameOrganization);
            Console.WriteLine("Бюджет на початку: " + MAC.Budjet);
            
            Console.WriteLine("Сотрудники: ");

            Employee Mari = new Employee("Danilova", "Mariia");
            Mari.ReturnInfo();

            Employee Lev = new Employee("Dow", "Lev");
            Lev.ReturnInfo();

            Post [] post = new Post[2];
            
            for (int i = 0; i < post.Length; i++)
            {
                post[i] = new Post();
            }

            post[0].nazvanieDoljnosti = "BackEnd";
            post[0].zarplata = 2000;
            post[0].sotrydnik = Mari;
            post[1].nazvanieDoljnosti = "FrontEnd";
            post[1].zarplata = 1500;
            post[1].sotrydnik = Lev;

            MAC.doljnosti = post;
            
            MAC.VuplataZarplat();

            Console.WriteLine("Статус проєкту: continues");
            Console.WriteLine("Поточний бюджет (за місяць): ");
            MAC.Budjet += MAC.RabotaMesac("continues");
            Console.WriteLine(MAC.Budjet);

            Console.ReadKey();
        }
    }
}
